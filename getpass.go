package main

import (
    "fmt"
    "regexp"
    "os"
    "os/exec"
    "io/ioutil"
    "net/http"
    "encoding/json"
)

type settingsType map[string]string

func main () {

    configFile, err := ioutil.ReadFile("/etc/getpass/config.json")

    if err != nil {
        fmt.Printf("%s", err)
        os.Exit(1)
    }

    var settings settingsType
    err = json.Unmarshal(configFile, &settings)

    if err != nil {
        fmt.Printf("%s", err)
        os.Exit(1)
    }

    req, err := http.NewRequest("GET", settings["url"], nil)

    if err != nil {
      fmt.Printf("%s", err)
      os.Exit(1)
    }

    req.SetBasicAuth(settings["key"], "X-Redmine-API-Key")

    client := http.Client{}
    res, err := client.Do(req)

    if err != nil {
      fmt.Printf("%s", err)
      os.Exit(1)
    }

    body, err := ioutil.ReadAll(res.Body)
    res.Body.Close()

    if err != nil {
      fmt.Printf("%s", err)
      os.Exit(1)
    }

    var f interface{}
    err = json.Unmarshal(body, &f)

    if err != nil {
      fmt.Printf("%s", err)
      os.Exit(1)
    }

    password := regexp.MustCompile("password: (.*)\n").FindStringSubmatch(f.(map[string]interface{})["wiki_page"].(map[string]interface{})["text"].(string))[1]

    echoPass := exec.Command("echo", password)
    copyPass := exec.Command("xclip", "-selection", "clipboard")

    copyPass.Stdin, _ = echoPass.StdoutPipe()
    copyPass.Stdout = os.Stdout
    _ = copyPass.Start()
    _ = echoPass.Run()
    _ = copyPass.Wait()
}

