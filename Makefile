default: build

build:
	go build -o getpass getpass.go

install:
	#clean build
	mkdir -p /etc/getpass
	cp config.json.dist /etc/getpass/config.json
	cp getpass /usr/local/bin

clean:
	if [ -f getpass ] ; then rm getpass ; fi;

uninstall:
	rm -rf /etc/getpass
	rm -rf /usr/local/bin/getpass
